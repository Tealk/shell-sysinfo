# shell-sysinfo

Displays system information of the Linux host

## Extensions/improvements
Create a report [here](https://codeberg.org/Tealk/shell-sysinfo/issues) and surprise me with your ideas.

## Bug Report
Feel free to create a report [here](https://codeberg.org/Tealk/shell-sysinfo/issues)
